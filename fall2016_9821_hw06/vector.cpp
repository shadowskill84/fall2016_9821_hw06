/*

*/

#include "vector.hpp"
#include <iostream>

namespace nla
{
	Vector::Vector(const int dim) : container_size(dim)
	{
		if (container_size > 0) container = new T[container_size];

		for (int i = 0; i < container_size; ++i)
			container[i] = 0;
	}

	Vector::Vector(const Vector& source) : container_size(source.container_size)
	{
		if (container_size > 0) container = new T[container_size];

		for (int i = 0; i < container_size; ++i)
			container[i] = source.container[i];
	}
	/*
	Vector::Vector(const std::vector<T>& source) : container_size(source.size())
	{
		container = new T[container_size];

		for (int i = 0; i < container_size; ++i)
			container[i] = source[i];
	}
	*/
	Vector& Vector::operator = (const Vector& source)
	{
		//preclude self-assignment
		if (this == &source)
			return *this;

		if (container_size > 0) delete[] container;
		container_size = source.container_size;
		if (container_size > 0) container = new T[container_size];
		
		for (int i = 0; i < container_size; ++i)
			container[i] = source.container[i];

		return *this;
	}
	
	Vector::~Vector()
	{
		if (container_size > 0) delete[] container;
	}

	T& Vector::operator [] (const int index)
	{
		//When the index is out of bounds, throw OutOfBoundsException object
		if ((index < 0) || (index >= container_size))
		{
			std::cout << "[]: Out of bounds error!" << std::endl;
			throw 0;
		}

		return container[index];
	}

	const T& Vector::operator [] (const int index) const
	{
		//When the index is out of bounds, throw OutOfBoundsException object
		if ((index < 0) || (index >= container_size))
		{
			std::cout << "[] const: Out of bounds error!" << std::endl;
			throw 0;
		}

		return container[index];
	}

	T& Vector::operator () (const int index)
	{
		//When the index is out of bounds, throw OutOfBoundsException object
		if ((index < 0) || (index >= container_size))
		{
			std::cout << "(): Out of bounds error!" << std::endl;
			throw 0;
		}

		return container[index];
	}

	const T& Vector::operator () (const int index) const
	{
		//When the index is out of bounds, throw OutOfBoundsException object
		if ((index < 0) || (index >= container_size))
		{
			std::cout << "() const: Out of bounds error!" << std::endl;
			throw 0;
		}

		return container[index];
	}

	int Vector::size() const
	{
		return container_size;
	}

	T Vector::sum(const int n) const
	{
		//When the index is out of bounds, throw OutOfBoundsException object
		if (n > container_size)
		{
			std::cout << "Sum: Out of bounds error!" << std::endl;
			throw 0;
		}

		T sum = 0;

		for (int i = 0; i < n; ++i)
			sum += container[i];

		return sum;
	}

	T Vector::sum(const int start, const int n) const
	{
		//When the index is out of bounds, throw OutOfBoundsException object
		if (start+n > container_size)
		{
			std::cout << "Sum: Out of bounds error!" << std::endl;
			throw 0;
		}

		T sum = 0;

		for (int i = start; i < start+n; ++i)
			sum += container[i];

		return sum;
	}


	void Vector::push_back(const T& element)
	{
		T* new_container = new T[++container_size];

		for (int i = 0; i < container_size-1; ++i)
			new_container[i] = container[i];

		new_container[container_size - 1] = element;
		if (container_size > 1) delete[] container;
		container = new_container;
	}

	//ostream overloader; send contents of Vector to ostream
	std::ostream& operator << (std::ostream& os, const Vector& source)
	{
		std::string sep(", ");

		if (source.size() == 0) os << "Empty!";
		else
		{
			for (int j = 0; j < source.size() - 1; ++j)
				os << source(j) << sep;
			os << source(source.size() - 1);
		}

		return os;
	}
	
	Vector Vector::operator * (const double multiplier)
	{
		Vector scaled(container_size);

		for (int i = 0; i < container_size; ++i)
			scaled[i] = multiplier * this->operator[](i);

		return scaled;
	}

	const Vector Vector::operator * (const double multiplier) const
	{
		Vector scaled(container_size);

		for (int i = 0; i < container_size; ++i)
			scaled[i] = multiplier * this->operator[](i);

		return scaled;
	}

	Vector Vector::operator + (const Vector& addend)
	{
		Vector sum(container_size);

		for (int i = 0; i < container_size; ++i)
			sum[i] = this->operator[](i)+addend[i];

		return sum;
	}

	const Vector Vector::operator + (const Vector& addend) const
	{
		Vector sum(container_size);

		for (int i = 0; i < container_size; ++i)
			sum[i] = this->operator[](i) + addend[i];

		return sum;
	}

	T max(const Vector& v)
	{
		T max = v[0];

		for (int i = 1; i < v.size(); ++i)
			if (v[i]>max) max = v[i];

		return max;
	}
}
/*
basic vector class
*/


#ifndef vector_HPP
#define vector_HPP

#include <sstream>

namespace nla
{
	typedef double T;

	class Vector
	{
	private:
		T* container;
		int container_size;

	public:
		//default and parameterized constructor
		explicit Vector(const int dim = 3);

		//copy constructor
		Vector(const Vector& source);
		
		//destructor
		virtual ~Vector();

		//assignment operator
		Vector& operator = (const Vector& source);

		int size() const;
		T sum(const int n) const;
		T sum(const int start, const int n) const;
		void push_back(const T& element);

		T& operator [] (const int index);
		const T& operator [] (const int index) const;
		T& operator () (const int index);
		const T& operator () (const int index) const;
		Vector operator * (const double multiplier);
		const Vector operator * (const double multiplier) const;
		Vector operator + (const Vector& addend);
		const Vector operator + (const Vector& addend) const;
	};

	//ostream overloader; send contents of Vector to ostream
	std::ostream& operator << (std::ostream& os, const Vector& source);

	//max value in vector
	T max(const Vector& v);
}

#endif

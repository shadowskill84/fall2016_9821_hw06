/*

*/

#include "vector.hpp"
#include "option.hpp"

namespace nla
{
	typedef nla::Vector vector;
	typedef nla::Option option;

	double S(const option& opt, const double z)
	{
		return opt.S * std::exp((opt.r - opt.q - opt.sig*opt.sig / 2.0)*opt.T + opt.sig*std::sqrt(opt.T)*z);
	}

	vector S(const option& opt, const vector z)
	{
		vector Si(z.size());

		for (int i = 0; i < Si.size(); ++i)
			Si[i] = S(opt, z[i]);

		return Si;
	}

	double C(const option& opt, const double S)
	{
		return std::exp(-opt.r*opt.T)*nla::max(0, S - opt.K);
	}

	double delta_call(const option& opt, const double S)
	{
		if (S > opt.K) return std::exp(-opt.r*opt.T) * S / opt.S;
		else return 0;
	}

	double vega_call(const option& opt, const double S, const double z)
	{
		if (S > opt.K) return S * std::exp(-opt.r*opt.T) * (-opt.sig*opt.T + std::sqrt(opt.T) * z);
		else return 0;
	}

	vector C(const option& opt, const vector S)
	{
		vector Ci(S.size());

		for (int i = 0; i < Ci.size(); ++i)
			Ci[i] = C(opt, S[i]);

		return Ci;
	}

	vector delta_call(const option& opt, const vector S)
	{
		vector deltai(S.size());

		for (int i = 0; i < deltai.size(); ++i)
			deltai[i] = delta_call(opt, S[i]);

		return deltai;
	}

	vector vega_call(const option& opt, const vector S, const vector z)
	{
		vector vegai(S.size());

		for (int i = 0; i < vegai.size(); ++i)
			vegai[i] = vega_call(opt, S[i], z[i]);

		return vegai;
	}


	double P(const option& opt, const double S)
	{
		return std::exp(-opt.r*opt.T)*nla::max(0, opt.K - S);
	}

	double delta_put(const option& opt, const double S)
	{
		if (opt.K > S) return -std::exp(-opt.r*opt.T) * S / opt.S;
		else return 0;
	}

	double vega_put(const option& opt, const double S, const double z)
	{
		if (opt.K > S) return -S * std::exp(-opt.r*opt.T) * (-opt.sig*opt.T + std::sqrt(opt.T) * z);
		else return 0;
	}

	vector P(const option& opt, const vector S)
	{
		vector Pi(S.size());

		for (int i = 0; i < Pi.size(); ++i)
			Pi[i] = P(opt, S[i]);

		return Pi;
	}

	vector delta_put(const option& opt, const vector S)
	{
		vector deltai(S.size());

		for (int i = 0; i < deltai.size(); ++i)
			deltai[i] = delta_put(opt, S[i]);

		return deltai;
	}

	vector vega_put(const option& opt, const vector S, const vector z)
	{
		vector vegai(S.size());

		for (int i = 0; i < vegai.size(); ++i)
			vegai[i] = vega_put(opt, S[i], z[i]);

		return vegai;
	}


	double _hat(const vector& v, const int N)
	{
		return v.sum(N) / N;
	}

	double _hat(const vector& v, const int start, const int N)
	{
		return v.sum(start, N) / (N-start);
	}

	double C_hat(const vector& C, const int N)
	{
		return C.sum(N) / N;
	}

	double delta_C_hat(const vector& delta_C, const int N)
	{
		return delta_C.sum(N) / N;
	}

	double vega_C_hat(const vector& vega_C, const int N)
	{
		return vega_C.sum(N) / N;
	}


	double P_hat(const vector& P, const int N)
	{
		return P.sum(N) / N;
	}

	double delta_P_hat(const vector& delta_P, const int N)
	{
		return delta_P.sum(N) / N;
	}

	double vega_P_hat(const vector& vega_P, const int N)
	{
		return vega_P.sum(N) / N;
	}
}
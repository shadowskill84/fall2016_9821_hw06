/*

*/

#include "mesh.hpp"

namespace nla
{
	Mesh::Mesh(const int dim1, const int dim2) : mesh_dim1(dim1), mesh_dim2(dim2)
	{
		container = new double*[dim1];

		for (int i = 0; i < dim1; ++i)
			container[i] = new double[dim2];

	}

	Mesh::Mesh(const Mesh& source) : mesh_dim1(source.mesh_dim1), mesh_dim2(source.mesh_dim2)
	{
		container = new double*[mesh_dim1];

		for (int i = 0; i < mesh_dim1; ++i)
		{
			container[i] = new double[mesh_dim2];
			for (int j = 0; j < mesh_dim2; ++j)
				container[i][j] = source.container[i][j];
		}
	}

	Mesh& Mesh::operator = (const Mesh& source)
	{
		//preclude self-assignment
		if (this == &source)
			return *this;

		for (int i = 0; i < mesh_dim1; ++i)
			delete[] container[i];

		delete[] container;

		mesh_dim1 = source.mesh_dim1;
		mesh_dim2 = source.mesh_dim2;

		container = new double*[mesh_dim1];

		for (int i = 0; i < mesh_dim1; ++i)
		{
			container[i] = new double[mesh_dim2];
			for (int j = 0; j < mesh_dim2; ++j)
				container[i][j] = source.container[i][j];
		}

		return *this;
	}

	Mesh::~Mesh()
	{
		for (int i = 0; i < mesh_dim1; ++i)
			delete[] container[i];

		delete[] container;
	}
	
	double& Mesh::operator () (const int index1, const int index2)
	{
		return container[index1][index2];
	}

	const double& Mesh::operator () (const int index1, const int index2) const
	{
		return container[index1][index2];
	}

	int Mesh::size1() const
	{
		return mesh_dim1;
	}

	int Mesh::size2() const
	{
		return mesh_dim2;
	}
	
	//ostream overloader; send contents of Mesh data to ostream
	std::ostream& operator << (std::ostream& os, const Mesh& source)
	{
		std::string sep(", ");

		for (int j = 0; j < source.size1(); ++j)
		{
			os << source(j, 0);

			for (int k = 1; k < source.size2(); ++k)
			{
				os << sep << source(j, k);
			}

			os << std::endl;
		}
				
		return os;
	}
}
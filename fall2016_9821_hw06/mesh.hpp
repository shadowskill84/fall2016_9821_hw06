/*
basic mesh class
*/


#ifndef mesh_HPP
#define mesh_HPP

#include <sstream>

namespace nla
{
	class Mesh
	{
	private:
		double** container;
		int mesh_dim1;
		int mesh_dim2;

	public:
		//default and parameterized constructor
		explicit Mesh(const int dim1 = 3, const int dim2 = 3);

		//copy constructor
		Mesh(const Mesh& source);

		//destructor
		virtual ~Mesh();

		//assignment operator
		Mesh& operator = (const Mesh& source);

		int size1() const;
		int size2() const;
		double& operator () (const int index1, const int index2);
		const double& operator () (const int index1, const int index2) const;
	};

	//ostream overloader; send contents of Mesh to ostream
	std::ostream& operator << (std::ostream& os, const Mesh& source);
}

#endif

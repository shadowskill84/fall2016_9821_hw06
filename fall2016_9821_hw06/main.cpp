/*

*/

#include <iostream>
#include <fstream>
#include "option.hpp"
#include "vector.hpp"
#include "mesh.hpp"		//for "reports"
#include "pseudo_rng.hpp"
#include "monte_carlo.hpp"

void monte_for_vanillas(const std::string& filename);
void monte_for_path_dependent(const std::string& filename);
void rng_comparisons(const std::string& filename);
//*********************************************************************************************************
void antithetic(const std::string& filename, const int k);
void moment_matching_and_control_variates(const std::string& filename, const int k);
void control_variate(const std::string& filename, const int k);
void moment_matching(const std::string& filename, const int k);
void monte_for_basket(const std::string& filename, const int k);
void monte_for_path_basket(const std::string& filename, const int k);
void monte_for_heston(const std::string& filename);

int main()
{
	std::string sep("****************************************************************************************************\n\n\n\n");
	
	/*
	nla::vector u = nla::linear_congruential_generator(4);
	nla::vector u2 = u+u;
	std::cout << u << "\n\n" << u2 << "\n\n";
	*/

	/*
	//hw06: problem 1 {finished}
	std::cout << "HW06: Monte Carlo Pricing and Greeks Estimations for Plain Vanilla European Options\n";
	monte_for_vanillas("monte_for_vanillas.csv");
	std::cout << sep;

	//hw06: problem 2
	std::cout << "HW06: Monte Carlo Pricing of a Path-Dependent Option";
	monte_for_path_dependent("monte_for_path_dependent.csv");
	std::cout << sep;

	//hw06: problem 3
	std::cout << "HW06: Comparison of Random Number Generators\n";
	rng_comparisons("rng_comparisons.csv");
	std::cout << sep;
	*/
	//*********************************************************************************************************
	
	const int max_k = 1;

	//hw07: problem 1 (part 1)
	std::cout << "HW07: Antithetic Variables\n";
	antithetic("antithetic.csv", 9);
	std::cout << sep;

	//hw07: problem 1 (part 2)
	std::cout << "HW07: Simultaneous Moment Matching and Control Variates\n";
	moment_matching_and_control_variates("moment_matching_and_control_variates.csv", max_k);
	std::cout << sep;

	//hw07: problem 1 (part 2)
	std::cout << "HW07: Control Variates\n";
	control_variate("control_variate.csv", max_k);
	std::cout << sep;

	//hw07: problem 1 (part 2)
	std::cout << "HW07: Moment Matching\n";
	moment_matching("moment_matching.csv", max_k);
	std::cout << sep;

	//hw07: problem 2
	std::cout << "HW07: Monte Carlo Pricing for Basket Options\n";
	monte_for_basket("monte_for_basket.csv", max_k);
	std::cout << sep;

	//hw07: problem 3
	std::cout << "HW07: Monte Carlo Pricing for Path-Dependent Basket Options\n";
	monte_for_path_basket("monte_for_path_basket.csv", 3);
	std::cout << sep;

	//hw07: problem 4
	std::cout << "HW07: Monte Carlo Simulation for the Heston Model\n";
	monte_for_heston("monte_for_heston.csv");
	std::cout << sep;
	
	return 0;
}

void monte_for_vanillas(const std::string& filename)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;
	
	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision); 
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);
	
	//homework parameters
	double S = 40.0, K = 42.0, T = 9.0 / 12.0, sig = 0.25, q = 0.01, r = 0.03;
	const option opt1(S, K, T, sig, q, r);
	const int N = 5120000;

	//1. Generate N independent samples from the standard normal distribution by using the Inverse Transform Method.
	const vector z = nla::inverse_transform(N);	//z[i] for i=0..N-1

	//2. Compute the Black�Scholes values of the call and put options, as well as the values of the	Delta and vega of the options
	double C_bs = my::bs_call(opt1);
	double delta_C_bs = my::bs_delta_call(opt1);
	double vega_C_bs = my::bs_vega(opt1);
	//std::cout << C_bs << "\t" << delta_C_bs << '\t' << vega_C_bs << "\n";
	
	double P_bs = my::bs_put(opt1);
	double delta_P_bs = my::bs_delta_put(opt1);
	double vega_P_bs = my::bs_vega(opt1);
	//std::cout << P_bs << "\t" << delta_P_bs << '\t' << vega_P_bs << "\n";

	//3. Use the N independent samples zi, i = 1 : N from the standard normal distribution obtained above to find approximate values of the options and of their Delta and vega using Monte Carlo simulations
	vector Si = mc::S(opt1, z);
	vector Ci = mc::C(opt1, Si);
	vector deltaCi = mc::delta_call(opt1, Si);
	vector vegaCi = mc::vega_call(opt1, Si, z);
	vector Pi = mc::P(opt1, Si);
	vector deltaPi = mc::delta_put(opt1, Si);
	vector vegaPi = mc::vega_put(opt1, Si, z);

	//Report values
	matrix C_report(7, 10);
	matrix P_report(7, 10);

	//populate report matrices column-wise
	int n;
	for (int k = 0; k < 10; ++k)
	{
		n = 10000 * long int(std::pow(2, k));
		/*
		output << "N";
		output << "\nC^(N)";
		output << "\nSqrt{N} |C_BS - C^(N)|";
		output << "\nDelta^C(N)";
		output << "\nSqrt{N} |Delta_BS - Delta^|";
		output << "\nVega^C(N)";
		output << "\nSqrt{N} |Vega_BS - Vega^|";
		*/
		C_report(0, k) = n;
		C_report(1, k) = mc::C_hat(Ci, n);
		C_report(2, k) = std::sqrt(n) * std::abs(C_bs - C_report(1, k));
		C_report(3, k) = mc::delta_C_hat(deltaCi, n);
		C_report(4, k) = std::sqrt(n) * std::abs(delta_C_bs - C_report(3, k));
		C_report(5, k) = mc::vega_C_hat(vegaCi, n);
		C_report(6, k) = std::sqrt(n) * std::abs(vega_C_bs - C_report(5, k));
		/*output << "\n\n\nN";
		output << "\nP^(N)";
		output << "\nSqrt{N} |P_BS - P^(N)|";
		output << "\nDelta^P(N)";
		output << "\nSqrt{N} |Delta_BS - Delta^|";
		output << "\nVega^P(N)";
		output << "\nSqrt{N} |Vega_BS - Vega^|";
		*/
		P_report(0, k) = n;
		P_report(1, k) = mc::P_hat(Pi, n);
		P_report(2, k) = std::sqrt(n) * std::abs(P_bs - P_report(1, k));
		P_report(3, k) = mc::delta_P_hat(deltaPi, n);
		P_report(4, k) = std::sqrt(n) * std::abs(delta_P_bs - P_report(3, k));
		P_report(5, k) = mc::vega_P_hat(vegaPi, n);
		P_report(6, k) = std::sqrt(n) * std::abs(vega_P_bs - P_report(5, k));
	}

	output << C_report << "\n\n" << P_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void monte_for_path_dependent(const std::string& filename)
{

}

void rng_comparisons(const std::string& filename)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	double S = 50.0, K = 55.0, T = 6.0 / 12.0, sig = 0.30, q = 0.00, r = 0.04;
	const option opt1(S, K, T, sig, q, r);
	const int N = 5120000;

	//Compute the Black�Scholes values of the put option, as well as the values of the Delta and vega of the option
	double P_bs = my::bs_put(opt1);
	//std::cout << "BS Put: " << P_bs << "\n";

	//Inverse Transform Method.
	const vector z = nla::inverse_transform(N);	//z[i] for i=0..N-1
	vector Si = mc::S(opt1, z);
	vector Pi = mc::P(opt1, Si);
	matrix P_inverse_report(10, 3);

	//populate report matrix column-wise
	for (int k = 0; k < 10; ++k)
	{
		int n = 10000 * long int(std::pow(2, k));
		P_inverse_report(k, 0) = n;
		P_inverse_report(k, 1) = mc::P_hat(Pi, n);
		P_inverse_report(k, 2) = std::abs(P_bs - P_inverse_report(k, 1));
	}

	output << P_inverse_report << std::endl;
	std::cout << output.str() << std::endl;
	/*
	//Acceptance�Rejection Method.
	const vector z_ar = nla::inverse_transform(N);	//z[i] for i=0..N-1
	vector Si_ar = mc::S(opt1, z_ar);
	vector Pi_ar = mc::P(opt1, Si_ar);
	matrix P_a_r_report(10, 3);

	//populate report matrix column-wise
	for (int k = 0; k < 10; ++k)
	{
		int n = 10000 * long int(std::pow(2, k));
		P_a_r_report(k, 0) = n;
		P_a_r_report(k, 1) = mc::P_hat(Pi_ar, n);
		P_a_r_report(k, 2) = std::abs(P_bs - P_a_r_report(k, 1));
	}

	output << P_a_r_report << std::endl;
	std::cout << output.str() << std::endl;
	*/

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

//*********************************************************************************************************

void antithetic(const std::string& filename, const int k)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	double S = 50.0, K = 55.0, T = 6.0 / 12.0, sig = 0.30, q = 0.0, r = 0.04;
	const option opt1(S, K, T, sig, q, r);
	//const int k = 9;
	const int N = 10000 * long int(std::pow(2, k)); // 5120000;

	//Compute the Black�Scholes value of the put option
	double V_bs = my::bs_put(opt1);

	//Generate N independent samples from the standard normal distribution by using the Box Muller Method.
	const vector z1 = nla::inverse_transform(N);	//z1[i] for i=0..N-1
	const vector z2 = z1*(-1);						//z2[i] for i=0..N-1

	//3. Use the N independent samples zi, i = 1 : N from the standard normal distribution obtained above to find approximate values of the option
	vector Si1 = mc::S(opt1, z1);
	vector Si2 = mc::S(opt1, z2);
	vector Vi1 = mc::P(opt1, Si1);
	vector Vi2 = mc::P(opt1, Si2);

	//Report values
	matrix V_report(k+1, 3);

	//populate report matrices row-wise
	int n;
	for (int j = 0; j <= k; ++j)
	{
		n = 10000 * long int(std::pow(2, j));

		V_report(j, 0) = n;
		V_report(j, 1) = 0.5*(mc::_hat(Vi1, n) + mc::_hat(Vi2, n));
		V_report(j, 2) = std::abs(V_bs - V_report(j, 1));
	}

	output << "\nAntithetic Variables\nn,V^_AV(N),|V_BS - V^_AV(N)|\n" << V_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void moment_matching_and_control_variates(const std::string& filename, const int k)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	double S = 50.0, K = 55.0, T = 6.0 / 12.0, sig = 0.30, q = 0.0, r = 0.04;
	const option opt1(S, K, T, sig, q, r);
	//const int k = 4;
	const int N = 10000 * long int(std::pow(2, k)); // 5120000;

	//Compute the Black�Scholes value of the put option
	double V_bs = my::bs_put(opt1);

	//Generate N independent samples from the standard normal distribution by using the Box Muller Method.
	const vector z = nla::box_muller_fill(N);	//z[i] for i=0..N-1

	//3. Use the N independent samples zi, i = 1 : N from the standard normal distribution obtained above to find approximate values of the option
	vector Si = mc::S(opt1, z);
	
	//Report values
	matrix V_report(k + 1, 3);

	//populate report matrices row-wise
	int n;
	for (int j = 0; j <= k; ++j)
	{
		n = 10000 * long int(std::pow(2, j));

		double S_hat = mc::_hat(Si, n);
		vector Si_tilda = Si*(std::exp(r*T)* S / S_hat);
		vector Vi_tilda = mc::P(opt1, Si_tilda);
		double V_hat = mc::_hat(Vi_tilda, n);

		double numerator = 0.0, denominator = 0.0;
		for (int i = 0; i < n; ++i)
		{
			numerator += (Si_tilda[i] - std::exp(r*T)* S)*(Vi_tilda[i] - V_hat);
			denominator += (Si_tilda[i] - std::exp(r*T)* S)*(Si_tilda[i] - std::exp(r*T)* S);
		}
		double b_hat = numerator*1.0 / denominator;

		vector Wi(n);
		for (int i = 0; i < n; ++i)
			Wi[i] = Vi_tilda[i] - b_hat*(Si_tilda[i] - std::exp(r*T)*S);

		V_report(j, 0) = n;
		V_report(j, 1) = mc::_hat(Wi, n);
		V_report(j, 2) = std::abs(V_bs - V_report(j, 1));
	}

	output << "\nSimultaneous Moment Matching and Control Variates\nn,V^_CV;MM(N),|V_BS - V^_CV;MM(N)|\n" << V_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void control_variate(const std::string& filename, const int k)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	double S = 50.0, K = 55.0, T = 6.0 / 12.0, sig = 0.30, q = 0.0, r = 0.04;
	const option opt1(S, K, T, sig, q, r);
	//const int k = 2;
	const int N = 10000 * long int(std::pow(2, k)); // 5120000;

	//Compute the Black�Scholes value of the put option
	double V_bs = my::bs_put(opt1);
	
	//Generate N independent samples from the standard normal distribution by using the Box Muller Method.
	const vector z = nla::box_muller_fill(N);	//z[i] for i=0..N-1

	//3. Use the N independent samples zi, i = 1 : N from the standard normal distribution obtained above to find approximate values of the option
	vector Si = mc::S(opt1, z);
	vector Vi = mc::P(opt1, Si);
	
	//Report values
	matrix V_report(k+1, 3);

	//populate report matrices row-wise
	int n;
	for (int j = 0; j <= k; ++j)
	{
		n = 10000 * long int(std::pow(2, j));
		
		double S_hat = mc::_hat(Si, n);
		double V_hat = mc::_hat(Vi, n);

		double numerator = 0, denominator = 0;
		for (int i = 0; i < n; ++i)
		{
			numerator   += (Si[i] - S_hat)*(Vi[i] - V_hat);
			denominator += (Si[i] - S_hat)*(Si[i] - S_hat);
		}
		double b_hat = numerator / denominator;

		vector Wi(n);
		for (int i = 0; i < n; ++i)
			Wi[i] = Vi[i] - b_hat*(Si[i] - std::exp(r*T)*S);

		V_report(j, 0) = n;
		V_report(j, 1) = mc::_hat(Wi, n);
		V_report(j, 2) = std::abs(V_bs - V_report(j, 1));
	}

	output << "\nControl Variate Technique\nn,V^_CV(N),|V_BS - V^_CV(N)|\n" << V_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void moment_matching(const std::string& filename, const int k)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	double S = 50.0, K = 55.0, T = 6.0 / 12.0, sig = 0.30, q = 0.0, r = 0.04;
	const option opt1(S, K, T, sig, q, r);
	//const int k = 4;
	const int N = 10000 * long int(std::pow(2, k)); // 5120000;

	//Compute the Black�Scholes value of the put option
	double V_bs = my::bs_put(opt1);

	//Generate N independent samples from the standard normal distribution by using the Box Muller Method.
	const vector z = nla::box_muller_fill(N);		//z[i] for i=0..N-1

	//Use the N independent samples zi, i = 1 : N from the standard normal distribution obtained above to find approximate values of the option
	vector Si = mc::S(opt1, z);
	
	//Report values
	matrix V_report(k + 1, 3);

	//populate report matrices row-wise
	int n;
	for (int j = 0; j <= k; ++j)
	{
		n = 10000 * long int(std::pow(2, j));

		double S_hat = mc::_hat(Si, n);
		vector Si_tilda = Si*(std::exp(r*T) * S / S_hat);
		vector Vi_tilda = mc::P(opt1, Si_tilda);

		V_report(j, 0) = n;
		V_report(j, 1) = mc::_hat(Vi_tilda, n);
		V_report(j, 2) = std::abs(V_bs - V_report(j, 1));
	}

	output << "\nMoment Matching\nn,V^_MM(N),|V_BS - V^_MM(N)|\n" << V_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void monte_for_basket(const std::string& filename, const int k)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	double S1 = 25.0, S2 = 30.0, K = 50.0, T = 6.0 / 12.0, sig1 = 0.30, sig2 = 0.2, q = 0.0, r = 0.05, p = 0.35;
	const option opt1(S1, K, T, sig1, q, r);
	const option opt2(S2, K, T, sig2, q, r);
	//const int k = 2;
	const int N = 10000 * long int(std::pow(2, k)); // 5120000;
	
	//Generate 2*N independent samples from the standard normal distribution by using the Box Muller Method.
	const vector z = nla::box_muller_fill(2*N);	//z[i] for i=0..2*N-1

	//Use the 2*N independent samples zi, i = 1 : 2*N from the standard normal distribution obtained above to find approximate values of the option
	vector S1j(N);
	vector S2j(N);

	for (int j = 0; j < N/2; ++j)
	{
		S1j[j] = S1*std::exp((opt1.r - opt1.q - opt1.sig*opt1.sig / 2.0)*opt1.T + opt1.sig*std::sqrt(opt1.T)*z[2*j]);
		S2j[j] = S2*std::exp((opt2.r - opt2.q - opt2.sig*opt2.sig / 2.0)*opt2.T + opt2.sig*std::sqrt(opt2.T)*(p*z[2*j] + std::sqrt(1-p*p)*z[2*j+1]));
	}

	vector Vj = mc::C(opt1, S1j + S2j);

	//Report values
	matrix V_report(k + 1, 2);

	//populate report matrices row-wise
	int n;
	for (int j = 0; j <= k; ++j)
	{
		n = 10000 * long int(std::pow(2, j));

		V_report(j, 0) = n;
		V_report(j, 1) = mc::_hat(Vj, n);
	}

	output << "\nMonte Carlo Pricing for Basket Options\nN,V^(N)\n" << V_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void monte_for_path_basket(const std::string& filename, const int k)
{
	//custom classes and functions
	typedef nla::Vector vector;
	typedef nla::Mesh matrix;
	typedef nla::Option option;
	namespace my = nla;
	namespace mc = nla;

	//output precision
	const int precision = 16;
	std::cout << std::fixed;
	std::cout.precision(precision);
	std::stringstream output;
	output << std::fixed;
	output.precision(precision);

	//homework parameters
	const double S1 = 25.0, S2 = 30.0, K = 50.0, T = 6.0 / 12.0, sig1 = 0.30, sig2 = 0.2, q = 0.0, r = 0.05, p = 0.35;
	const option opt1(S1, K, T, sig1, q, r);
	const option opt2(S2, K, T, sig2, q, r);
	//const int k = 2;
	const int m = 150;
	const double dt = T / double(m);
	const int N = 50 * long int(std::pow(2, k));
	//std::cout << "N*m = " << N*m << std::endl;

	//Generate 2*N independent samples from the standard normal distribution by using the Box Muller Method.
	const vector z = nla::box_muller_fill(2 * N * m);	//z[i] for i=0..2*N-1

	//Use the 2*N independent samples zi, i = 1 : 2*N from the standard normal distribution obtained above to find approximate values of the option
	vector Vi(N);
	vector S1t(m);
	vector S2t(m);

	for (int j = 0; j < m*N; ++j)
	{
		if (j%m == 0)
		{
			S1t[0] = S1;
			S2t[0] = S2;
		}
		else
		{
			S1t[j%m] = S1t[(j%m) - 1] * std::exp((opt1.r - opt1.q - opt1.sig*opt1.sig / 2.0)*dt + opt1.sig*std::sqrt(dt)*z[2 * j]);
			S2t[j%m] = S2t[(j%m) - 1] * std::exp((opt2.r - opt2.q - opt2.sig*opt2.sig / 2.0)*dt + opt2.sig*std::sqrt(dt)*(p*z[2 * j] + std::sqrt(1 - p*p)*z[2 * j + 1]));

			if ((j + 1) % m == 0) Vi[(j + 1 - m) / m] = std::exp(-r*T) * nla::max(max(S1t + S2t) - K, 0);
		}

		//if (j%m == 0) std::cout << "Progress: path generator = " << j << " (" << j*100.0 / (m*N) << "%)" << std::endl;
	}

	//Report values
	matrix V_report(k + 1, 3);
	
	//populate report matrices row-wise
	int n;
	for (int j = 0; j <= k; ++j)
	{
		n = 50 * long int(std::pow(2, j));

		V_report(j, 0) = n;
		V_report(j, 1) = m;
		V_report(j, 2) = mc::_hat(Vi, n);
	}
	
	output << "\nMonte Carlo Pricing for Path-Dependent Basket Options\nn,m,V^(N)\n" << V_report << std::endl;
	std::cout << output.str() << std::endl;

	std::ofstream myfile;
	//std::string filename = __FUNCTION__ + std::string(".csv");
	myfile.open(filename);
	myfile << output.str() << std::endl;
	myfile.close();
}

void monte_for_heston(const std::string& filename)
{

}

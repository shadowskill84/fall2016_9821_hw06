/*

*/

#include "option.hpp"

namespace nla
{
	double PI()
	{
		return atan(1.0) * 4.0;
	}

	//
	double max(const double arg1, const double arg2)
	{
		if (arg1>arg2)
			return arg1;

		return arg2;
	}

	//standard cumulative normal (Gaussian) distribution function
	double N(const double x)
	{
		double cdf = 0.5*erfc(-x / sqrt(2.0));
		return cdf;
	}

	//black-scholes pricers
	double bs_call(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double d2 = d1 - sig * std::sqrt(T);
		double cprice = S * std::exp((b - r)*T) * N(d1) - K * std::exp(-r*T) * N(d2);

		return cprice;
	}

	double bs_put(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double d2 = d1 - sig * std::sqrt(T);
		double pprice = -S * std::exp((b - r)*T) * N(-d1) + K * std::exp(-r*T) * N(-d2);

		return pprice;
	}

	double bs_call(const Option& option)
	{
		return bs_call(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	double bs_put(const Option& option)
	{
		return bs_put(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	//black-scholes greeks
	double bs_delta_call(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double cdelta = std::exp((b - r)*T)*N(d1);

		return cdelta;
	}

	double bs_delta_put(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double pdelta = -std::exp((b - r)*T)*N(-d1);

		return pdelta;
	}

	double bs_gamma(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double d1 = (std::log(S / K) + (r - q + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double gamma = std::exp(-d1*d1 / 2.0) * std::exp(-q*T) / (S*sig*std::sqrt(2 * PI()*T));

		return gamma;
	}

	double bs_theta_call(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double d2 = d1 - sig * std::sqrt(T);
		return -((S*sig*std::exp(-q*T)*std::exp(-d2*d2 / 2.0)) / (2 * std::sqrt(2 * PI()*T))) + (q*S*std::exp(-q*T)*N(d1)) - r*K*std::exp(-r*T)*N(d2);
	}

	double bs_theta_put(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double d2 = d1 - sig * std::sqrt(T);
		return (-(S*sig*std::exp(-q*T)*std::exp(-d1*d1 / 2.0)) / (2 * std::sqrt(2 * PI()*T))) - (q*S*std::exp(-q*T)*N(-d1)) + r*K*std::exp(-r*T)*N(-d2);
	}

	double bs_vega(const double S, const double K, const double T, const double sig, const double q, const double r)
	{
		double b = r - q;
		double d1 = (std::log(S / K) + (b + sig*sig / 2.0)*T) / (sig * std::sqrt(T));
		double vega = S*std::exp(-q*T)*std::sqrt(T)*std::exp(-d1*d1/2) / std::sqrt(2*PI());

		return vega;
	}

	double bs_delta_call(const Option& option)
	{
		return bs_delta_call(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	double bs_delta_put(const Option& option)
	{
		return bs_delta_put(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	double bs_gamma(const Option& option)
	{
		return bs_gamma(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	double bs_theta_call(const Option& option)
	{
		return bs_theta_call(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	double bs_theta_put(const Option& option)
	{
		return bs_theta_put(option.S, option.K, option.T, option.sig, option.q, option.r);
	}

	double bs_vega(const Option& option)
	{
		return bs_vega(option.S, option.K, option.T, option.sig, option.q, option.r);
	}
	
	//default and parameterized constructor
	Option::Option(const double S, const double K, const double T, const double sig, const double q, const double r) 
		: S(S), K(K), T(T), sig(sig), q(q), r(r) {}

	//copy constructor
	Option::Option(const Option& source) : S(source.S), K(source.K), T(source.T), sig(source.sig), q(source.q), r(source.r) {}

	//destructor
	Option::~Option()	{}

	//assignment operator
	Option& Option::operator = (const Option& source)
	{
		//preclude self-assignment
		if (this == &source)
			return *this;

		S = source.S;
		K = source.K;
		T = source.T;
		sig = source.sig;
		q = source.q;
		r = source.r;
		
		return *this;
	}

	//ostream overloader; send contents of Option data to ostream
	std::ostream& operator << (std::ostream& os, const Option& source)
	{
		char* sep = ", ";
		os << "S: " << source.S << sep << "K: " << source.K << sep << "T: " << source.T << sep << "sig: " << source.sig << sep << "q: " << source.q << sep << "r: " << source.r;

		return os;
	}
}

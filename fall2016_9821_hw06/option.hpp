/*
basic option class
*/


#ifndef option_HPP
#define option_HPP

#include <sstream>

namespace nla
{
	//class to encapsulate all option data in one place (protected member data of Option class)
	class Option
	{
	public:
		double S;		//spot price
		double K;		//strike price
		double T;		//exercise (maturity) date
		double sig;		//constant volatility
		double q;		//dividend yield
		double r;		//risk-free interest rate
		
		//default and parameterized constructor
		explicit Option(const double S = 41, const double K = 40, const double T = 1, const double sig = 0.3, const double q = 0.01, const double r = 0.03);

		//copy constructor
		Option(const Option& source);

		//destructor
		virtual ~Option();

		//assignment operator
		Option& operator = (const Option& source);
	};

	//ostream overloader; send contents of Option data to ostream
	std::ostream& operator << (std::ostream& os, const Option& option);

	double PI();

	//standard cumulative normal (Gaussian) distribution function
	double N(const double x);

	//max between 2 numbers
	double max(const double arg1, const double arg2);

	//black-scholes pricers
	double bs_call(const double S, const double K, const double T, const double sig, const double q, const double r);				//
	double bs_put(const double S, const double K, const double T, const double sig, const double q, const double r);				//
	double bs_call(const Option& option);
	double bs_put(const Option& option);

	//black-scholes greeks
	double bs_delta_call(const double S, const double K, const double T, const double sig, const double q, const double r);			//
	double bs_delta_put(const double S, const double K, const double T, const double sig, const double q, const double r);			//
	double bs_gamma(const double S, const double K, const double T, const double sig, const double q, const double r);				//
	double bs_theta_call(const double S, const double K, const double T, const double sig, const double q, const double r);			//
	double bs_theta_put(const double S, const double K, const double T, const double sig, const double q, const double r);			//
	double bs_vega(const double S, const double K, const double T, const double sig, const double q, const double r);
	double bs_delta_call(const Option& option);
	double bs_delta_put(const Option& option);
	double bs_gamma(const Option& option);
	double bs_theta_call(const Option& option);
	double bs_theta_put(const Option& option);
	double bs_vega(const Option& option);
}

#endif

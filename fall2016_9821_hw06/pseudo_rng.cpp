/*

*/

//#include <cmath> //abs, log, exp, sqrt (already include from <sstream> in vector.hpp
#include "pseudo_rng.hpp"
#include <iostream>
//#include <vector>

namespace nla
{
	typedef nla::Vector vector;

	//Beasley�Springer�Moro algorithm to compute F_inverse(u) where F is cdf of standard normal
	double F_inv(const double u)
	{
		double 
			a0 = 2.50662823884,		b0 = -8.47351093090,
			a1 = -18.61500062529,	b1 = 23.08336743743,
			a2 = 41.39119773534,	b2 = -21.06224101826,
			a3 = -25.44106049637,	b3 = 3.13082909833,
			c0 = 0.3374754822726147, c5 = 0.0003951896511919,
			c1 = 0.9761690190917186, c6 = 0.0000321767881768,
			c2 = 0.1607979714918209, c7 = 0.0000002888167364,
			c3 = 0.0276438810333863, c8 = 0.0000003960315187,
			c4 = 0.0038405729373609;
		
		double x, r, y = u - 0.5;

		if (std::abs(y) < 0.42)
		{
			r = y * y;
			x = y * (((a3 * r + a2) * r + a1) * r + a0) / ((((b3 * r + b2) * r + b1) * r + b0) * r + 1);
		}
		else
		{
			r = u;
			if (y > 0) r = 1 - u;
			r = std::log(-std::log(r));
			x = c0 + r * (c1 + r * (c2 + r * (c3 + r  * (c4 + r * (c5 + r * (c6 + r * (c7 + r * c8)))))));
			if (y < 0) x = -x;
		}

		return x;
	}

	//Generate N independent samples from the uniform distribution on [0, 1] by using the Linear Congruential Generator
	vector linear_congruential_generator(const int N)
	{
		vector u(N);
		const int x0 = 1, a = 39373, c = 0;
		const int k = 2147483647; // std::pow(2, 31) - 1;
		unsigned long long int x = x0;

		for (int i = 0; i < N; ++i)
		{
			x = (a * x + c) % k;
			u[i] = double(x)/k;
		}

		return u;
	}

	//Generate N independent samples from the standard normal distribution by using	the independent uniform samples
	vector inverse_transform(const int N)
	{
		vector z(N);
		vector u = linear_congruential_generator(N);

		for (int i = 0; i < N; ++i)
			z[i] = F_inv(u[i]);

		return z;
	}

	//Generate N independent samples from the standard normal distribution by using	the independent uniform samples
	vector inverse_transform(const vector u)
	{
		int N = u.size();
		vector z(N);

		for (int i = 0; i < N; ++i)
			z[i] = F_inv(u[i]);

		return z;
	}

	//Generate N independent samples from the standard normal distribution by using the	independent uniform samples
	vector acceptance_rejection(const int N)
	{
		vector u = linear_congruential_generator(3 * N);
		//std::vector<T> z;
		vector z(0);

		for (int i = 0; i < 3*N; i+=3)
		{
			double x = -std::log(u[i]);
			if (u[i + 1] <= std::exp(-0.5*(x - 1)*(x - 1)))
			{
				if (u[i + 2] <= 0.5) x = -x;
				z.push_back(x);
			}
		}

		return z;
	}

	//Generate N independent samples from the standard normal distribution by using the	independent uniform samples
	vector box_muller(const int N)
	{
		//vector holding N uniform samples
		vector u = linear_congruential_generator(N);

		//empty vector which will hold resulting normal samples
		vector z(0);
		
		// Marsaglia�Bray algorithm
		for (int i = 1; i < N; i += 2)
		{
			T u1 = 2 * u[i - 1] - 1, u2 = 2 * u[i] - 1;
			T x = u1*u1 + u2*u2;
			if (x <= 1)
			{
				T y = std::sqrt(-2.0*std::log(x) / x);
				T z1 = u1*y, z2 = u2*y;
				z.push_back(z1);
				z.push_back(z2);
			}
		}

		return z;
	}

	//Generate N independent samples from the standard normal distribution by using enough independent uniform samples
	vector box_muller_fill(const int N)
	{
		//empty vector which will hold resulting normal samples
		vector z(0);

		const int x0 = 1, a = 39373, c = 0;
		const int k = 2147483647; // std::pow(2, 31) - 1;
		unsigned long long int x = x0;

		while (z.size() < N)
		{
			T u1, u2;
			
			x = (a * x + c) % k;
			u1 = double(x) / k;

			x = (a * x + c) % k;
			u2 = double(x) / k;

			u1 = 2 * u1 - 1;
			u2 = 2 * u2 - 1;
			T X = u1*u1 + u2*u2;
			if (X <= 1)
			{
				T y = std::sqrt(-2.0*std::log(X) / X);
				T z1 = u1*y;
				T z2 = u2*y;
				z.push_back(z1);
				z.push_back(z2);
			}

			//if (z.size() % 10000 == 0) std::cout << "Progress: box muller size = " << z.size() << " (" << z.size()*100.0/N << "%)" << std::endl;
		}

		return z;
	}
}

/*

*/

#ifndef pseudo_rng_hpp
#define pseudo_rng_hpp

#include "vector.hpp"

namespace nla
{
	typedef nla::Vector vector;

	double F_inv(const double u);						//Beasley�Springer�Moro algorithm to compute F_inverse(u) where F is cdf of standard normal

	vector linear_congruential_generator(const int N);	//Generate N independent samples from the uniform distribution on [0, 1] by using the Linear Congruential Generator
	vector inverse_transform(const int N);				//Generate N independent samples from the standard normal distribution by using	the independent uniform samples
	vector inverse_transform(const vector u);			//Generate N independent samples from the standard normal distribution by using	the independent uniform samples
	vector acceptance_rejection(const int N);			//Generate N independent samples from the standard normal distribution by using the	independent uniform samples
	vector acceptance_rejection(const vector u);		//Generate N independent samples from the standard normal distribution by using the	independent uniform samples
	vector box_muller(const int N);						//Generate independent samples from the standard normal distribution by using N independent uniform samples
	vector box_muller(const vector u);					//Generate independent samples from the standard normal distribution by using N independent uniform samples

	vector box_muller_fill(const int N);				//Generate N independent samples from the standard normal distribution by using enough independent uniform samples
}
#endif // !pseudo_rng_hpp

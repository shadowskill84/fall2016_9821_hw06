/*

*/

#ifndef monte_carlo_hpp
#define monte_carlo

#include "vector.hpp"
#include "option.hpp"

namespace nla
{
	typedef nla::Vector vector;
	typedef nla::Option option;
	
	double S(const option& opt, const double z);
	vector S(const option& opt, const vector z);
	
	double C(const option& opt, const double S);
	double delta_call(const option& opt, const double S);
	double vega_call(const option& opt, const double S, const double z);

	vector C(const option& opt, const vector S);
	vector delta_call(const option& opt, const vector S);
	vector vega_call(const option& opt, const vector S, const vector z);

	double P(const option& opt, const double S);
	double delta_put(const option& opt, const double S);
	double vega_put(const option& opt, const double S, const double z);

	vector P(const option& opt, const vector S);
	vector delta_put(const option& opt, const vector S);
	vector vega_put(const option& opt, const vector S, const vector z);

	double _hat(const vector& v, const int N);
	double _hat(const vector& v, const int start, const int N);

	double C_hat(const vector& C, const int N);
	double delta_C_hat(const vector& delta_C, const int N);
	double vega_C_hat(const vector& vega_C, const int N);

	double P_hat(const vector& P, const int N);
	double delta_P_hat(const vector& delta_P, const int N);
	double vega_P_hat(const vector& vega_P, const int N);
}

#endif
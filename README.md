# README #

### Class ###
* MTH 9821 Numerical Methods for Finance
* Homework 06
* Homework 07

### Authors ###
* Gye Hyun Baek
* Yuchen Qi
* Christopher Thomas <christopher.thomas.06@gmail.com>
* Haoyuan Yu
* Mengqi Zhang

### Script Summary ###
This program will print the necessary table information for each Assignment segment.

Libraries needed: iostream, fstream, sstream.

### Usage ###
Built using Visual Studio Express 2013. Execute: fall2016_9821_hw06.sln

### Assignment ###

HW06: Monte Carlo Pricing and Greeks Estimations for Plain Vanilla European Options

HW06: Monte Carlo Pricing of a Path-Dependent Option

HW06: Comparison of Random Number Generators

HW07: Antithetic Variables

HW07: Simultaneous Moment Matching and Control Variates

HW07: Monte Carlo Pricing for Basket Options

HW07: Monte Carlo Pricing for Path-Dependent Basket Options

HW07: Monte Carlo Simulation for the Heston Model